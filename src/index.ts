import {
    popularFilmsAction,
    setFavriteMovieOnceAction
} from './typescript/actions/filmsActions';
import eventsService from './typescript/sevices/eventsService';

export async function render(): Promise<void> {
    // TODO render your app here
    await popularFilmsAction();
    await setFavriteMovieOnceAction();
    document.addEventListener('click', eventsService);
}

export enum events {
  popular = '#popular',
  upcoming = '#upcoming',
  topRated = '#top_rated',
  loadMore = 'load-more',
  submit = 'submit'
};
export enum containers {
  films = 'film-container',
  randomMovie = 'random-movie',
  favoriteMovies = 'favorite-movies',
  search = '#search',
  dataId = 'data-id'
};
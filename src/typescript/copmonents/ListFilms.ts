import { IFilm } from '../interfaces/IFilm';
import { containers } from '../enum/containers';
import Card from './Card';

const wrapperForCard = (film: IFilm) => `
  <div class="col-lg-3 col-md-4 col-12 p-2" ${containers.dataId}="${film.id}">
    ${Card(film)}
  </div>
`

const ListFilms = (listFilms: IFilm[]): string => listFilms.map(film => wrapperForCard(film)).join('');

export default ListFilms;

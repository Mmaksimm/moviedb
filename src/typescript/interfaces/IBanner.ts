export interface IBanner {
  overview?: string;
  title?: string;
}
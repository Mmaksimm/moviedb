export interface IQueryFetch {
  endpoint?: string;
  page?: number;
  query?: string;
}
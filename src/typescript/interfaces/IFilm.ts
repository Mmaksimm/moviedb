export interface IFilm {
  id: number;
  title: string;
  overview: string;
  release_date: string;
  poster_path: string;
  like?: boolean;
};
import { IFilm } from '../interfaces/IFilm';
import { events } from '../enum/events';
import { containers } from '../enum/containers';
import {
  popularFilms,
  topRatedFilms,
  upcomingMovies,
  filmByName,
  filmById,
} from '../sevices/filmService';
import setFilmInfo from '../helpers/setFilmInfo';
import filmInfoFilter from '../helpers/filmInfoFilter';
import {
  getFilms,
  setListFavoriteMovies,
  getLikeFilmsId,
  toggleLikeById
} from '../store';

interface ILikeParent {
  id: string;
}

const popularFilmsAction = async (page = 1): Promise<void> => {
  const results = await popularFilms(page);
  await window.location.replace(`${events.popular}/${page}`);
  setFilmInfo(page, results);
}

const topRatedAction = async (page = 1): Promise<void> => {
  const results = await topRatedFilms(page);
  await window.location.replace(`${events.topRated}/${page}`);
  setFilmInfo(page, results);
}

const upcomingMoviesAction = async (page = 1): Promise<void> => {
  const results = await upcomingMovies(page);
  await window.location.replace(`${events.upcoming}/${page}`);
  setFilmInfo(page, results);
}

const searchFilmAction = async (page = 1): Promise<void> => {
  const input = await document.getElementById(containers.search) as HTMLElement;
  const query = input.value.trim() as string;
  const results = await filmByName({ query, page });
  await window.location.replace(`${containers.search}/${page}`);
  setFilmInfo(page, results);
}

const setFavriteMovieOnceAction = async (): Promise<void> => {
  const listFilms = getFilms();
  const listIdLikeFilms = getLikeFilmsId();

  let listLikeFilms: IFilm[] = [];

  for (const idLikeFilm of listIdLikeFilms) {
    let likeFilm = listFilms.find(({ id }: { id: number }) => id === idLikeFilm);
    if (!likeFilm) {
      likeFilm = await filmById(idLikeFilm);
      likeFilm = filmInfoFilter(likeFilm as IFilm, listIdLikeFilms);
    }
    listLikeFilms = [...listLikeFilms, likeFilm];
  };

  setListFavoriteMovies(listLikeFilms);
}

const loadMoreAction = async (): Promise<void> => {
  const [actions, page] = window.location.hash.split('/');
  switch (actions) {
    case events.popular:
      await popularFilmsAction(+page + 1);
      break;

    case events.upcoming:
      await upcomingMoviesAction(+page + 1);
      break;

    case events.topRated:
      await topRatedAction(+page + 1);
      break;

    case containers.search:
      await searchFilmAction(+page + 1);
      break;

  }
}

const likeToggleAction = (target: EventTarget): void | undefined => {
  const parent = target.closest(`[${containers.dataId}]`) as HTMLElement;
  if (!parent?.dataset?.id) return
  const id = parent.dataset.id as unknown as ILikeParent;
  toggleLikeById(Number(id));
}

export {
  popularFilmsAction,
  topRatedAction,
  upcomingMoviesAction,
  loadMoreAction,
  likeToggleAction,
  searchFilmAction,
  setFavriteMovieOnceAction
};
import { events } from '../enum/events';
import { containers } from '../enum/containers';

import {
  popularFilmsAction,
  upcomingMoviesAction,
  topRatedAction,
  loadMoreAction,
  likeToggleAction,
  searchFilmAction
} from '../actions/filmsActions';


interface IEventsService {
  (event: MouseEvent): void | never
}

let oldId = '';

const eventsService: IEventsService = event => {
  try {
    const { target } = event;
    if (!target) return;

    const id = target.id || '' as string;
    if (!id && id === oldId && id !== events.loadMore) return;
    if (id) oldId = id as string;

    const currentId = window.location.hash.split('/')[0];
    if (currentId === id && !(id === events.submit && currentId === containers.search)) return;

    switch (id) {
      case events.popular:
        popularFilmsAction();
        break;

      case events.upcoming:
        upcomingMoviesAction();
        break;

      case events.topRated:
        topRatedAction();
        break;

      case events.loadMore:
        loadMoreAction();
        break;

      case events.submit:
        searchFilmAction()
        break;

      default:
        likeToggleAction(target);
    }
  } catch (err) {
    alert(err.message)
  }
}

export default eventsService;

import { IQueryFetch } from '../interfaces/IQueryFetch';
import { IFilm } from '../interfaces/IFilm';
import callWebApi from '../helpers/webApiHelper';

const filmByName = async ({ query, page }: IQueryFetch): Promise<IFilm[] | never> => {
  const { results } = await callWebApi({
    endpoint: 'search/movie',
    query,
    page
  });
  return results;
};

const filmById = async (id: number): Promise<IFilm | never> => {
  const response = await callWebApi({
    endpoint: `movie/${id}`
  }) as IFilm;
  return response
};

const popularFilms = async (page = 1): Promise<IFilm[] | never> => {
  const { results } = await callWebApi({
    endpoint: 'movie/popular',
    page
  });
  return results;
};

const topRatedFilms = async (page = 1): Promise<IFilm[] | never> => {
  const { results } = await callWebApi({
    endpoint: 'movie/top_rated',
    page
  });
  return results;
};

const upcomingMovies = async (page = 1): Promise<IFilm[] | never> => {
  const { results } = await callWebApi({
    endpoint: 'movie/upcoming',
    page
  });
  return results;
};

export {
  filmByName,
  filmById,
  popularFilms,
  topRatedFilms,
  upcomingMovies,
}


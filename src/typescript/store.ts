import { IBanner } from './interfaces/IBanner';
import { IFilm } from './interfaces/IFilm';
import { IFilmUpdate } from './interfaces/IFilmUpdate';
import { TLikeFilmsId } from './types/TLikeFilmsId';

import randomFilmInfo from './helpers/randomFilmInfo';
import filmInfoFilter from './helpers/filmInfoFilter';
import {
  renderlistFilms,
  renderBannerFilm,
  renderFilmById,
  renderFavoriteMovies
} from './render';


let listFilms: IFilm[] | never[] = [];
let favoriteMovies: IFilm[] | never[] = [];
let bannerFilm: IBanner = {};
let likeFilmsId = JSON.parse(localStorage.getItem('likeFilmsId') as string) as number[] | undefined;

const setStorageLikeFilms = (likeFilmsId: TLikeFilmsId) => localStorage.setItem('likeFilmsId', JSON.stringify(likeFilmsId));

if (!likeFilmsId) {
  likeFilmsId = [];
  setStorageLikeFilms(likeFilmsId)
}

//Like Films Id
const getLikeFilmsId = (): TLikeFilmsId => [...likeFilmsId as TLikeFilmsId];

const getLikeFilmIdById = (id: number) => likeFilmsId?.find(likeFilmId => likeFilmId === id);

const addLikeFilmId = (id: number) => {
  likeFilmsId = [...likeFilmsId as TLikeFilmsId, id];
};

const deleteLikeFilmId = (id: number) => {
  if (likeFilmsId && likeFilmsId.length) {
    likeFilmsId = likeFilmsId.filter((likeId: number) => id !== likeId);
  }
}

// Favorite Movies
const setListFavoriteMovies = (listFavoriteMoviesVale: IFilm[]): void => {
  favoriteMovies = [...listFavoriteMoviesVale];
  renderFavoriteMovies(favoriteMovies);
};

const addFavoriteMovies = (likeFilm: IFilm) => {
  favoriteMovies = [{ ...likeFilm, like: true }, ...favoriteMovies];
}

const deleteFavoriteMovies = (id: number) => {
  favoriteMovies = favoriteMovies.filter(film => id !== film.id);
}

// Films
const getFilms = (): IFilm[] => [...listFilms];

const getFilmById = (id: number): IFilm | undefined => listFilms
  .find(({ id: filmId }) => filmId === id);

const setFilms = (films: IFilm[] | undefined): void => {
  if (!films || !films.length) {
    listFilms = [{ id: 0, title: '', overview: '', release_date: '', poster_path: '', like: false }]
    bannerFilm = { overview: '', title: '' };
  } else {
    listFilms = films.map(film => filmInfoFilter(film, likeFilmsId as TLikeFilmsId));
    bannerFilm = randomFilmInfo(films);
  }
  renderBannerFilm(bannerFilm);
  renderlistFilms(listFilms);
};

const addFilms = (films: IFilm[]): void => {
  const newlistFilms = films.map((film: IFilm) => filmInfoFilter(film, likeFilmsId as TLikeFilmsId));
  listFilms = [...listFilms, ...newlistFilms]
  renderlistFilms(listFilms);
}

const updateFilmById = async (id: number, data: IFilmUpdate) => {
  listFilms = listFilms.map(film => film.id !== id
    ? film
    : ({ ...film, ...data })
  )
};

// Toggle Like
const toggleLikeById = async (id: number): Promise<void> => {
  if (getLikeFilmIdById(id)) {
    updateFilmById(id, { like: false })
    deleteLikeFilmId(id);
    deleteFavoriteMovies(id)
    const updatFilm = getFilmById(id);
    if (updatFilm) renderFilmById(updatFilm);
  } else {
    updateFilmById(id, { like: true });
    addLikeFilmId(id);
    const updatFilm = getFilmById(id) as IFilm;
    addFavoriteMovies(updatFilm);
    renderFilmById(updatFilm);
  }
  setStorageLikeFilms(getLikeFilmsId());
  await renderFavoriteMovies(favoriteMovies);
}

export {
  getLikeFilmsId,
  setListFavoriteMovies,
  setFilms,
  addFilms,
  getFilmById,
  getFilms,
  toggleLikeById
}
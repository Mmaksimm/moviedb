import { IFilm } from '../interfaces/IFilm';

const filmInfoFilter = ({ id, title, overview, release_date, poster_path }: IFilm, likeFilms: number[]): IFilm => (
  { id, title, overview, release_date, poster_path, like: likeFilms.some(likeId => id === likeId) }
);

export default filmInfoFilter;
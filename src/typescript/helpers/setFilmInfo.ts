import { IFilm } from '../interfaces/IFilm';
import { setFilms, addFilms } from '../store';

const setFilmInfo = (page: number, films: IFilm[]): void => {
  if (page === 1) {
    setFilms(films);
  } else {
    addFilms(films);
  }
}

export default setFilmInfo
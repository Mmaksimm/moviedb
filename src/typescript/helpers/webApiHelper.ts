import { IQueryFetch } from '../interfaces/IQueryFetch';

const movieDbPath = 'https://api.themoviedb.org/3/'

function getFetchUrl({ endpoint, page, query }: IQueryFetch) {
  const token = process.env.TOKEN as string;
  return `${movieDbPath}${endpoint}?api_key=${token}&language=en-US${!page ? '' : `&page=${page}`}${!query ? '' : `&query=${query}`}`
}

export async function throwIfResponseFailed(res: Response): Promise<void | never> {
  if (!res.ok) {
    let parsedException = 'Something went wrong with request!';
    try {
      parsedException = await res.json();
    } catch (err) {
      //
    }
    throw parsedException;
  }
}

export default async function callWebApi<Type>(args: IQueryFetch): Promise<Type | never> {
  const res = await fetch(getFetchUrl(args));
  await throwIfResponseFailed(res);
  return res.json();
}

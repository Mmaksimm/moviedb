import { IFilm } from '../interfaces/IFilm';
import { IBanner } from '../interfaces/IBanner';

const randomFilmInfo = (films: IFilm[]): IBanner => {
  let randomIndex = Math.round(20 * Math.random());
  randomIndex = Math.max(0, randomIndex);
  randomIndex = Math.min(20, randomIndex, films.length - 1);
  const { title, overview } = films[randomIndex] as IFilm;
  return { title, overview }
};

export default randomFilmInfo;

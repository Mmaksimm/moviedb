import { IFilm } from './interfaces/IFilm';
import { IBanner } from './interfaces/IBanner';
import { containers } from './enum/containers';
import ListFilms from './copmonents/ListFilms';
import Banner from './copmonents/Banner';
import Card from './copmonents/Card';
import ListFavoriteMovies from './copmonents/ListFavoriteMovies';

const filmContainer = document.getElementById(containers.films) as HTMLElement;
const filmBanner = document.getElementById(containers.randomMovie) as HTMLElement;
const favoriteMovies = document.getElementById(containers.favoriteMovies) as HTMLElement;

const renderlistFilms = (listFilms: IFilm[]): void => { filmContainer.innerHTML = ListFilms(listFilms) };

const renderBannerFilm = (bannerFilm: IBanner): void => { filmBanner.innerHTML = Banner(bannerFilm) };

const renderFilmById = async (film: IFilm): Promise<void> => {
  const filmElement = await document.querySelectorAll(`[data-id="${film.id}"]`);
  filmElement[filmElement.length - 1].innerHTML = Card(film);
};

const renderFavoriteMovies = (listFavoriteMovies: IFilm[]): void => {
  favoriteMovies.innerHTML = ListFavoriteMovies(listFavoriteMovies);
}

export {
  renderlistFilms,
  renderBannerFilm,
  renderFilmById,
  renderFavoriteMovies
}
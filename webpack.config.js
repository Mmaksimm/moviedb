/* eslint-disable */
const path = require('path');
const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const devMode = !process.env.NODE_ENV || process.env.NODE_ENV !== 'production'

let settings = {
  entry: ['babel-polyfill', './index.ts'],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json']
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              configFile: './babel.config.js',
              cacheDirectory: true
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader'
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|ico)$/i,
        type: 'asset/resource',
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: 'index.html',
      inject: 'body'
    })
  ],
  mode: devMode ? 'development' : 'production',
  devServer: {
    inline: true,
    contentBase: path.join(__dirname, 'public'),
    port: 9090
  },
  devtool: 'source-map',
};

if (devMode) {
  settings = {
    ...settings,
    plugins: [
      ...settings.plugins,
      new Dotenv()
    ]
  }
}

if (!devMode) {
  settings = {
    ...settings,
    plugins: [
      ...settings.plugins,
      new MiniCssExtractPlugin({
        filename: '[name].css'
      }),
      new webpack.DefinePlugin({
        'process.env.TOKEN': JSON.stringify(process.env.TOKEN),
      })
    ]
  }
}

module.exports = settings;
